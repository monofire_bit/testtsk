//
//  TTOrder.h
//  TestTsk
//
//  Created by ax on 10/27/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>



@interface TTOrder : NSObject

// order address
@property (strong, nonatomic,readonly) NSDictionary *departureAddress;
@property (strong, nonatomic,readonly) NSDictionary *destinationAddress;

// google location for address
@property (strong, nonatomic,readonly) CLLocation *departureAddressLocation;
@property (strong, nonatomic,readonly) CLLocation *destinationAddressLocation;

// google decoded address
@property (strong, nonatomic,readonly) NSString *departureDecodedAddress;
@property (strong, nonatomic,readonly) NSString *destinationDecodedAddress;

// flags to manage UI operations
@property (nonatomic) BOOL hasDepartureMarkerOnMap;
@property (nonatomic) BOOL hasDestinationMarkerOnMap;


// init order instance with dicts / set default values
- (id)initWithDepartureAddress: (NSDictionary*) departureAddressDictionary
         andDestinationAddress: (NSDictionary*) destinationAddressDictionary;

@end
