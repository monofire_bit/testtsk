//
//  AppDelegate.h
//  TestTsk
//
//  Created by ax on 10/26/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

