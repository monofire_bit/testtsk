//
//  TTTabBarController.m
//  TestTsk
//
//  Created by ax on 10/26/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import "TTTabBarController.h"
#import "TTViewController.h"
#import "TTLayoutConstraints.h"

#define kMoreNavigationControllerTitle @"ИСЧО"
#define kPicturesArray @[@"monster1",@"monster2",@"monster3",@"monster4",@"monster5",@"monster6"]
#define kNamesArray @[@"нечто",@"гугло",@"чо?",@"ктулхо",@"(*#чо",@"#@*!чо"]

@interface TTTabBarController ()

@end

@implementation TTTabBarController

#pragma mark
#pragma mark - Controller Life Cycle

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self setUpTabBarAppearance];
    [self enumerateChildViewControllers];
    [self swapTabBarItemLabel:@"More" to:kMoreNavigationControllerTitle];
    self.customizableViewControllers = nil;
    self.moreNavigationController.navigationBar.topItem.title=kMoreNavigationControllerTitle;
}



#pragma mark
#pragma mark - Child View Controllers Appearance

// enumerate children and add titles, labels, etc
-(void) enumerateChildViewControllers{
    
    NSArray *controllersNames=kNamesArray;
    NSArray *pictureNames=kPicturesArray;
    
    // take top level controller
    for (NSInteger i=0; i<self.viewControllers.count; i++) {
        
        // set title
        id child = [self.viewControllers objectAtIndex:i];
        [child setTitle:[controllersNames objectAtIndex:i]];
        
        // look though split controllers branch
        if ([child isKindOfClass:NSClassFromString(@"TTSplitViewController")]) {
            
            // look though nav controllers
            for (id navController in [child childViewControllers]) {
                
                // look though view controllers
                for (id rootViewController in [navController childViewControllers]) {
                    
                    // remove backButton label
                    [self setBackButtonTitle:@"" forViewController:rootViewController];
                    
                    // set title
                    [rootViewController setValue:[controllersNames objectAtIndex:i] forKey:@"title"];
                    
                    // add label to all view controllers except mapViewController (with google map)
                    if (![rootViewController isKindOfClass:NSClassFromString(@"TTMapViewController")]) {
                        [self addLabelWithTitle:[controllersNames objectAtIndex:i] andImageName:[pictureNames objectAtIndex:i] toViewController:rootViewController];
                    }
                }
            }
        }
        
        // look though nav controllers branch
        if ([child isKindOfClass:NSClassFromString(@"UINavigationController")]) {
            
            // look though view controllers
            for (UIViewController *rootViewController in [child childViewControllers]) {
                
                // remove backButton label
                UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
                barButton.title = @"";
                self.moreNavigationController.navigationBar.topItem.backBarButtonItem = barButton;
                
                // set title
                [rootViewController setValue:[controllersNames objectAtIndex:i] forKey:@"title"];
                
                // add label
                [self addLabelWithTitle:[controllersNames objectAtIndex:i] andImageName:[pictureNames objectAtIndex:i] toViewController:rootViewController];
            }
        }
    }
}


#pragma mark
#pragma mark - Controller Appearance Tools

// removes "back" title
-(void) setBackButtonTitle: (NSString*) titel forViewController: (UIViewController *) viewController{
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"";
    viewController.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
}

// creates and add label to view controller
-(void) addLabelWithTitle: (NSString*) labelTitle andImageName: (NSString*) imageName toViewController: (UIViewController*) viewController{
    
    // create label
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(viewController.view.bounds.size.width/2-75,
                                                              viewController.view.bounds.size.height/2,
                                                              150,
                                                              15)];
    
    // prepare image text attribute
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image =  [self imageWithImage:[UIImage imageNamed:imageName] scaledToSize:CGSizeMake(12, 12)];
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *titleString= [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@",labelTitle]];
    NSMutableAttributedString *resultString = [[NSMutableAttributedString alloc]init];
    
    [resultString appendAttributedString:attachmentString];
    [resultString appendAttributedString:titleString];
    
    // set label params
    [label setTintColor:[UIColor blackColor]];
    [label setAttributedText:resultString];
    [label setTextAlignment:NSTextAlignmentCenter];
    [viewController.view addSubview:label];
    
    // add label constraints
    [TTLayoutConstraints constraintForLabel:label];
}



#pragma mark
#pragma mark - Tab Bar Appearance
// changes "more" to "ISTCHO"
- (void) swapTabBarItemLabel: (NSString*) initialLabel to: (NSString*) newLabel{
    
    // look through tabBar elements
    for (id tapBarElement in [self.tabBar subviews]) {
        
        // get tabBarButton element
        if ([tapBarElement isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            
            // look through tabBarButton elements
            for (id tapBarButtonElement in [tapBarElement subviews]) {
                
                // get tabBarButtonLabel element
                if ([tapBarButtonElement isKindOfClass:NSClassFromString(@"UITabBarButtonLabel")]) {
                    
                    // swap initial label text on custom value
                    if ([[tapBarButtonElement text] isEqualToString:initialLabel]) {
                        
                        // set label frame and constraints
                        [TTLayoutConstraints constraintForButtonLabel:tapBarButtonElement];
                        [tapBarButtonElement setFrame:CGRectMake(0, 0, 39.5, 15)];
                        
                        // set label text and alignment
                        [tapBarButtonElement setText:newLabel];
                        [tapBarButtonElement setTextAlignment:NSTextAlignmentCenter];
                    }
                }
            }
        }
    }
}


-(void) setUpTabBarAppearance{
    // add pictures
    NSArray *pictureNames=kPicturesArray;
    for (NSInteger i=0 ; i<pictureNames.count; i++) {
        UITabBarItem *tabBarItem =[[self.viewControllers objectAtIndex:i]tabBarItem];
        [tabBarItem setImage: [self imageWithImage:[UIImage imageNamed:[pictureNames objectAtIndex:i]] scaledToSize:CGSizeMake(30, 30)]];
    }
    
    // remove "edit" button for "more" nav controller
    self.customizableViewControllers = nil;
    // change title or "more" nav controller
    self.moreNavigationController.navigationBar.topItem.title=kMoreNavigationControllerTitle;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark
#pragma mark - Device Rotation
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
        // redraw after rotation (override system layouts of "More"), relevant for iPhone 6
        [self swapTabBarItemLabel:@"More" to:kMoreNavigationControllerTitle];
        self.customizableViewControllers = nil;
        
        if ([self.moreNavigationController.navigationBar.topItem.title isEqualToString:@"More"]) {
             self.moreNavigationController.navigationBar.topItem.title=kMoreNavigationControllerTitle;
        }
        
    } completion:nil];
}

@end
