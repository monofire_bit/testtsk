//
//  TTOrder.m
//  TestTsk
//
//  Created by ax on 10/27/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import "TTOrder.h"

@interface TTOrder  ()

@property (strong, nonatomic,readwrite) NSDictionary *departureAddress;
@property (strong, nonatomic,readwrite) NSDictionary *destinationAddress;

@property (strong, nonatomic,readwrite) CLLocation *departureAddressLocation;
@property (strong, nonatomic,readwrite) CLLocation *destinationAddressLocation;

@property (strong, nonatomic,readwrite) NSString *departureDecodedAddress;
@property (strong, nonatomic,readwrite) NSString *destinationDecodedAddress;


@end


@implementation TTOrder

#pragma mark -
#pragma mark - Init
- (id)initWithDepartureAddress: (NSDictionary*) departureAddressDictionary
         andDestinationAddress: (NSDictionary*) destinationAddressDictionary{
    
    if (self = [super init]) {
        self.departureAddress = departureAddressDictionary;
        self.destinationAddress = destinationAddressDictionary;
        self.destinationAddressLocation=nil;
        self.departureAddressLocation=nil;
        self.departureDecodedAddress=nil;
        self.destinationDecodedAddress=nil;
        self.hasDestinationMarkerOnMap=NO;
        self.hasDepartureMarkerOnMap=NO;
    }
    return self;
}


@end
