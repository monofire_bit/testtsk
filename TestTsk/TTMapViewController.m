//
//  TTMapViewController.m
//  TestTsk
//
//  Created by ax on 10/26/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import "TTMapViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "TTServerManager.h"
#import "TTOrder.h"


@import GoogleMaps;

@interface TTMapViewController ()<CLLocationManagerDelegate, GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *ordersArray;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;


@end

@implementation TTMapViewController



#pragma mark
#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.delegate=self;
    self.locationManager = [[CLLocationManager alloc]init];
    [self.locationManager setDelegate:self];
    [self.locationManager requestWhenInUseAuthorization];
    [self attachErrorsNotificationObserver];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    /* prevents conflict between autorisation dialog and possible google api error
     start google api operations (populate datasource) only after user's responce on autorisation request */
    if ( ![CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined) {
        // populate only if user defined auth status
        [self populateDataSource];
    }
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark
#pragma mark - Data Source

- (void) populateDataSource{
    
    // reset map markers
    [self clearMarkers];
    
    // populate OrdersArray if needed
    if (!self.ordersArray) {
        [[TTServerManager serverManagerSharedInstance]requestOrdersFromServerWithBlock:^(NSArray *ordersArray) {
            self.ordersArray = [[NSMutableArray alloc]initWithArray:ordersArray];
            [self updateMarkers];
        }];
    }else{
        [self updateMarkers];
    }
}

// update markers UI / load location data if needed
- (void) updateMarkers{
    
    //counter of processed order items
    __block NSInteger orderCount=1;
    __weak typeof(self)weakSelf = self;
    
    for ( TTOrder *orderItem in self.ordersArray) {
        
        if (!orderItem.destinationAddressLocation) {
            
            // request location
            [[TTServerManager serverManagerSharedInstance]requestLocationForOrder:orderItem
                                                                   andAddressType:DESTINATION_ADDRESS
                                                                         andBlock:^(NSDictionary *resultLocationDictionary) {
                                                                             
                                                                             // update UI
                                                                             [weakSelf addMarkerWithLocation:[resultLocationDictionary valueForKey:kLocationKey]
                                                                                                andColor:[UIColor redColor]
                                                                                                andLabel:[resultLocationDictionary valueForKey:kFormatted_addressKey]];
                                                                             
                                                                             weakSelf.progressView.progress = (double)orderCount/(double)(weakSelf.ordersArray.count*2);
                                                                             orderCount++;
                                                                             
                                                                             // update hasMarker flag
                                                                             orderItem.hasDestinationMarkerOnMap=YES;
                                                                             
                                                                             // update item properties
                                                                             [orderItem setValue: [resultLocationDictionary valueForKey:kLocationKey] forKey:@"destinationAddressLocation"];
                                                                             [orderItem setValue:[resultLocationDictionary valueForKey:kFormatted_addressKey] forKey:@"destinationDecodedAddress"];
                                                                             
                                                                         }];
            
        }
        
        if (!orderItem.departureAddressLocation) {
            
            // request location
            [[TTServerManager serverManagerSharedInstance]requestLocationForOrder:orderItem
                                                                   andAddressType:DEPARTURE_ADDRESS
                                                                         andBlock:^(NSDictionary *resultLocationDictionary) {
                                                                             
                                                                             // update UI
                                                                             [weakSelf addMarkerWithLocation:[resultLocationDictionary valueForKey:kLocationKey]
                                                                                                andColor:[UIColor greenColor]
                                                                                                andLabel:[resultLocationDictionary valueForKey:kFormatted_addressKey]];
                                                                             
                                                                             weakSelf.progressView.progress = (double)orderCount/(double)(weakSelf.ordersArray.count*2);
                                                                             orderCount++;
                                                                             
                                                                             // update hasMarker flag
                                                                             orderItem.hasDepartureMarkerOnMap=YES;
                                                                             
                                                                             // update item properties
                                                                             [orderItem setValue: [resultLocationDictionary valueForKey:kLocationKey] forKey:@"departureAddressLocation"];
                                                                             [orderItem setValue:[resultLocationDictionary valueForKey:kFormatted_addressKey] forKey:@"departureDecodedAddress"];
                                                                             
                                                                         }];
            
        }
        
        if (orderItem.destinationAddressLocation && !orderItem.hasDestinationMarkerOnMap) {
            
            // update UI
            [weakSelf addMarkerWithLocation:orderItem.destinationAddressLocation
                               andColor:[UIColor redColor]
                               andLabel:orderItem.destinationDecodedAddress];
            
            weakSelf.progressView.progress = (double)orderCount/(double)(weakSelf.ordersArray.count*2);
            orderCount++;
            
            
            // update hasMarker flag
            orderItem.hasDestinationMarkerOnMap=YES;
        }
        
        
        if (orderItem.departureAddressLocation && !orderItem.hasDepartureMarkerOnMap) {
            
            // update UI
            [weakSelf addMarkerWithLocation:orderItem.departureAddressLocation
                               andColor:[UIColor greenColor]
                               andLabel:orderItem.departureDecodedAddress];
            
            weakSelf.progressView.progress = (double)orderCount/(double)(weakSelf.ordersArray.count*2);
            orderCount++;
            
            
            // update hasMarker flag
            orderItem.hasDepartureMarkerOnMap=YES;
        }
        
    }
    
}

-(void) clearMarkers{
    
    // on map
    [self.mapView clear];
    
    // flags in collection
    for ( TTOrder *orderItem in self.ordersArray) {
        orderItem.hasDepartureMarkerOnMap=NO;
        orderItem.hasDestinationMarkerOnMap=NO;
    }
    
    // reset progress
    self.progressView.progress=0;
}

#pragma mark
#pragma mark - Markers

- (void) addMarkerWithLocation: (CLLocation*) location andColor: (UIColor*) markerColor andLabel: (NSString*) markerLabel{
    GMSMarker *marker = [GMSMarker markerWithPosition:location.coordinate];
    [marker setTitle:markerLabel];
    [marker setMap:self.mapView];
    [marker setIcon:[GMSMarker markerImageWithColor:markerColor]];
    [marker setOpacity:0.4];
    [marker setAppearAnimation:kGMSMarkerAnimationPop];
    
}


#pragma mark
#pragma mark - Delegates

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    if (status==kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
        self.mapView.myLocationEnabled=true;
        self.mapView.settings.myLocationButton=true;
        
        /* prevents conflict between autorisation dialog and possible google api error
         start google api operations (populate datasource) only after user's responce on autorisation request */
        [self populateDataSource];
    }else if (status==kCLAuthorizationStatusDenied){
        [self populateDataSource];
    }
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    /** uncomment if camera tracking required
     CLLocation *location = [locations firstObject];
     
     [self.mapView setCamera:[GMSCameraPosition cameraWithTarget:location.coordinate
     zoom:10
     bearing:0
     viewingAngle:0]];
     */
    [self.locationManager stopUpdatingLocation];
}


#pragma mark -
#pragma mark Notification Center

// LISTEN..........................
// tuned to an error notifications
-(void) attachErrorsNotificationObserver{
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(showAlertWithNotification:)
                                                name:ERROR_NOTIFICATION
                                              object:nil];
}


-(void) showAlertWithNotification:(NSNotification *) alertNotification{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Error!"
                                          message:alertNotification.userInfo[kNotififactionDict]
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *retryAction = [UIAlertAction
                                  actionWithTitle:@"Retry"
                                  style:UIAlertActionStyleCancel
                                  handler:^(UIAlertAction *action)
                                  {
                                      [self populateDataSource];
                                  }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                               }];
    
    [alertController addAction:retryAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}




#pragma mark
#pragma mark - Details view Controller

// show details on long press
- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    // get link on detail view controller from parent navigation controller
    UIViewController *destinationDetailViewController = [self.splitViewController valueForKey:@"childDetailViewController"];
    
    // present detail view controller
    [self showViewController:destinationDetailViewController sender:self];
    
}




#pragma mark
#pragma mark - Debug Tools
-(void) printOrdersArray{
    
    for (TTOrder *orderItem in self.ordersArray) {
        static NSInteger i=1;
        NSLog(@"-----------------------------%li of %lu",(long)i,(unsigned long)self.ordersArray.count);
        i++;
        
        NSLog(@"LOC %@, %@",orderItem.departureAddressLocation.description, orderItem.destinationAddressLocation.description);
        if (!orderItem.destinationAddressLocation || !orderItem.departureAddressLocation) {
            NSLog(@"**************************");
        }
        
        NSLog(@"FORMAT ADDRESS %@, %@",orderItem.departureDecodedAddress, orderItem.destinationDecodedAddress);
        NSLog(@"ADR %@, %@",orderItem.departureAddress.description, orderItem.destinationAddress.description);
    }
}


@end
