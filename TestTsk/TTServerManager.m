//
//  TTServerManager.m
//  TestTsk
//
//  Created by ax on 10/27/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import "TTServerManager.h"

@interface TTServerManager()

@property (nonatomic, strong) NSDictionary *countryCodeDictionary;

@end


@implementation TTServerManager

#pragma mark
#pragma mart - Lazy Init

// prepare country codes dict from plist
-(NSDictionary*) countryCodeDictionary{
    
    if (!_countryCodeDictionary) {
        NSString *path = [[NSBundle mainBundle] pathForResource: @"countryCodes" ofType: @"plist"];
        _countryCodeDictionary = [NSDictionary dictionaryWithObjects:[[NSDictionary dictionaryWithContentsOfFile: path]allKeys]
                                                             forKeys:[[NSDictionary dictionaryWithContentsOfFile: path]allValues]];
    }
    return _countryCodeDictionary;
}



#pragma mark -
#pragma mark Server Manager Init
// create shared instance
+(TTServerManager *) serverManagerSharedInstance{
    static TTServerManager *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance=[[TTServerManager alloc]init];
    });
    return sharedInstance;
}


#pragma mark -
#pragma mark Public Operations

// prepare Orders collection
-(void) requestOrdersFromServerWithBlock: (void (^)(NSArray *ordersArray)) requestSuccess{
    
    // prepare Orders resoursce URL
    NSURL *url = [NSURL URLWithString:[kResourceUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    __block NSMutableArray *resultOrdersArray = [[NSMutableArray alloc]init];
    
    // download orders data
    [self downloadDataFromServerWithUrl:url onServerDownloadSuccess:^(NSData *data) {
        
        //on download success  - create ASYNC execution context
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // convert data to JSON and create Orders array based on parsed JSON
            resultOrdersArray = [NSMutableArray arrayWithArray:[self createOrdersArrayWithJsonObject:[self prepareJsonFromData:data]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // call requestSuccess block with ordersArray result on main thread
                requestSuccess(resultOrdersArray);
                
            });
        });
    }];
}


//request geocding data from Google for certain Item from Order Collection
-(void) requestLocationForOrder: (TTOrder*) orderElement
                 andAddressType: (AddressType) addressType
                       andBlock: (void (^)(NSDictionary *resultLocationDictionary)) requestSuccess{
    
    NSURL *url;
    
    // create URL fro geocoding request
    if (addressType==DESTINATION_ADDRESS){
        url = [self createUrlWithAddress:orderElement.destinationAddress];
    } else{
        url = [self createUrlWithAddress:orderElement.departureAddress];
    }
    
    __block NSDictionary *locationResultsDictionary = [[NSDictionary alloc]init];
    
    // download geocoding data
    [self downloadDataFromServerWithUrl:url onServerDownloadSuccess:^(NSData *data) {
        
        //on download success  - create ASYNC execution context
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // convert geocoding data to JSON and create location  based on parsed JSON
            locationResultsDictionary =[self createLocationDataWithJsonObject:[self prepareJsonFromData:data]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // return location data to caller
                requestSuccess(locationResultsDictionary);
                
            });
        });
    }];
}




#pragma mark
#pragma mark - Geocoding Tools

// create geocoding request URL
-(NSURL*) createUrlWithAddress: (NSDictionary *) addressDictionary{
    
    // create requet string components:
    
    //convert 3 letter country code to 2 letter code
    NSString *componentsString = [NSString stringWithFormat:@"components=country:%@",
                                  [self shortCountryCodeWithLongCode:[addressDictionary valueForKey:kCountryCodeKey]]];

    NSString *keyString = [NSString stringWithFormat:@"key=%@",kGoogleServerKey];
    
    NSMutableString *addressString = [NSMutableString stringWithFormat:@"address=%@+%@",
                                      [addressDictionary valueForKey:kZipCodeKey],
                                      [addressDictionary valueForKey:kCityKey]];
    
    
    NSMutableString *addressSubString = [NSMutableString stringWithFormat:@"+"];
    
    
    // create pool of address components and add to address if not nil
    NSSet *addressKeys = [[NSSet alloc]initWithArray:@[kStreetKey,kHouseNumberKey]];
    
    BOOL hasLeadingComponent = NO;
    
    // enumerate address components
    for (id addressComponent in [addressDictionary allKeys]) {
        
        NSString *addressComponentValue = [addressDictionary valueForKey:addressComponent];
        
        if ([[NSSet setWithObject:addressComponent] isSubsetOfSet:addressKeys] && addressComponentValue.length>0) {
            
            if (hasLeadingComponent) {
                [addressSubString appendString:@"+"];
            }
            
            // append it to address string
            [addressSubString appendString:[addressDictionary valueForKey:addressComponent]];
            hasLeadingComponent=YES;
        }
    }
    
    // if no address components, add & ant the end string. otherwise add "&" sign at the end
    if (!hasLeadingComponent) {
        addressSubString=[NSMutableString stringWithFormat:@"&"];
    }else{
        [addressSubString appendString:@"&"];
    }
    
    // build request string
    NSString *resultString = [NSString stringWithFormat:@"%@%@%@%@&%@",kGoogleGeocodingBaseUrl,addressString,addressSubString,componentsString,keyString];
    
    // set encoding
    NSURL *resultUrl = [NSURL URLWithString:[resultString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    return resultUrl;
}


- (NSString*) shortCountryCodeWithLongCode: (NSString*) longCode{
    return [self.countryCodeDictionary valueForKey:longCode];
}



// returns dictionary with location object and resolved address
-(NSDictionary*) createLocationDataWithJsonObject: (id) jsonObject{
    
    NSMutableDictionary *resultLocationDictionary=[[NSMutableDictionary alloc]init];
    
    CLLocation *resultLocation;
    NSString *requestStatusCode = [jsonObject valueForKey:kStatusKey];
    
    // check responce status code
    if ([requestStatusCode isEqualToString:@"OK"]) {
        
        NSArray *resultsArray = [jsonObject valueForKey:kResultsKey];
        
        // browse JSON structure:
        
        // get formatted address
        NSString *formattedAddress = [[resultsArray objectAtIndex:0]valueForKey:kFormatted_addressKey];
        
        // get location coordinates
        NSDictionary *geometryDict = [[resultsArray objectAtIndex:0]valueForKey:kGeometryKey];
        NSDictionary *locationDict = [geometryDict valueForKey:kLocationKey];
        resultLocation=[[CLLocation alloc]initWithLatitude:[[locationDict valueForKey:kLatKey] floatValue]
                                                 longitude:[[locationDict valueForKey:kLngKey] floatValue]];
        
        // create result dictionary
        [resultLocationDictionary setValue:resultLocation forKey:kLocationKey];
        [resultLocationDictionary setValue:formattedAddress forKey:kFormatted_addressKey];
        
    }else {
        [self postErrorNotification:[NSString stringWithFormat:@"GEOCODING API ERROR! %@",requestStatusCode]];
    }
    
    return resultLocationDictionary;
}


#pragma mark
#pragma mark - JSON operations

-(id) prepareJsonFromData: (NSData *) data{
    NSError *error;
    id resultObject =[NSJSONSerialization JSONObjectWithData:data
                                                     options:kNilOptions
                                                       error:&error];
    if (error) {
        [self postErrorNotification:error.localizedDescription];
    }
    
    return resultObject;
}


#pragma mark
#pragma mark - Orders collection tools

// builds orders array from JSON
-(NSArray *) createOrdersArrayWithJsonObject: (id) jsonObject{
    
    NSMutableArray *resultArray=[[NSMutableArray alloc]init];
    
    // NOTE: if elements order matters, use "for" cycle, instead of enumeration
    for (id arrayElement in jsonObject) {
        
        // create order instance from array element
        TTOrder *orderElement = [[TTOrder alloc]initWithDepartureAddress:[arrayElement objectForKey:kDepartureAddressKey]
                                                   andDestinationAddress:[arrayElement objectForKey:kDestinationAddressKey]];
        
        // add order instance to result array
        [resultArray addObject:orderElement];
    }
    return resultArray;
}



#pragma mark -
#pragma mark Server Interchange

// download data from server
- (void) downloadDataFromServerWithUrl: (NSURL *) url
               onServerDownloadSuccess: (void (^) (NSData *data)) serverDownloadSuccess{
    
    // initiate download
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *requestedData,
                                NSURLResponse *response,
                                NSError *connectionError) {
                // success
                if ([requestedData length]>0 && connectionError ==nil){
                    serverDownloadSuccess(requestedData);
                    
                    // no data downloaded
                } else if ([requestedData length]==0 && connectionError == nil){
                    [self postErrorNotification:@"ERROR! NO DATA DOWNLOADED"];
                    
                    // download error
                } else if (connectionError !=nil){
                    [self postErrorNotification:connectionError.localizedDescription];
                }
            }] resume];
}




#pragma mark
#pragma mark - Notification Center

// POST..........................
// notify root controller about errors
-(void) postErrorNotification: (NSString*) errorNotification{
    NSLog(@"ERROR! %@",errorNotification );
    [[NSNotificationCenter defaultCenter] postNotificationName:ERROR_NOTIFICATION
                                                        object:self
                                                      userInfo:@{kNotififactionDict:errorNotification}];
}

@end




