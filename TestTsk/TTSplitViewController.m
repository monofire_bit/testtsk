//
//  TTSplitViewController.m
//  TestTsk
//
//  Created by ax on 10/27/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import "TTSplitViewController.h"

@interface TTSplitViewController () <UISplitViewControllerDelegate>

// link on childDetailViewController
@property (nonatomic, strong) UIViewController *childDetailViewController;

@end

@implementation TTSplitViewController

#pragma mark
#pragma mark - Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate=self;
    
    // get link on child viewController
    NSArray *childNavControllers  = [self childViewControllers];
    self.childDetailViewController = [[[childNavControllers objectAtIndex:1]childViewControllers]objectAtIndex:0];
}



#pragma mark
#pragma mark - Delegates
// launch split view with master controller presented
-(BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController{
    
    return YES;
}




@end
