//
//  TTLayoutConstraints.m
//  TestTsk
//
//  Created by ax on 10/27/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import "TTLayoutConstraints.h"

@implementation TTLayoutConstraints

// add label constrains
+ (void)constraintForLabel:(UILabel *)label{
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *labelCenterXConstraint = [NSLayoutConstraint constraintWithItem:label
                                                                              attribute:NSLayoutAttributeCenterX
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:label.superview
                                                                              attribute:NSLayoutAttributeCenterX
                                                                             multiplier:1
                                                                               constant:0];
    
    NSLayoutConstraint *labelCenterYConstraint = [NSLayoutConstraint constraintWithItem:label
                                                                              attribute:NSLayoutAttributeCenterY
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:label.superview
                                                                              attribute:NSLayoutAttributeCenterY
                                                                             multiplier:1
                                                                               constant:0];
    
    
    NSLayoutConstraint *labelWidthConstraint = [NSLayoutConstraint constraintWithItem:label
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:nil
                                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                                           multiplier:1
                                                                             constant:150];
    
    NSLayoutConstraint *labelHeightConstraint = [NSLayoutConstraint constraintWithItem:label
                                                                             attribute:NSLayoutAttributeHeight
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:nil
                                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                                            multiplier:1
                                                                              constant:15];
    
    
    [label.superview addConstraints:@[labelCenterXConstraint,labelCenterYConstraint,labelWidthConstraint,labelHeightConstraint]];
    
}

// more label constraints
+ (void)constraintForButtonLabel:(UILabel *)label{
    
    label.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *labelCenterXConstraint = [NSLayoutConstraint constraintWithItem:label
                                                                              attribute:NSLayoutAttributeCenterX
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:label.superview
                                                                              attribute:NSLayoutAttributeCenterX
                                                                             multiplier:1
                                                                               constant:0];
    
    
    NSLayoutConstraint *labelBottomConstraint = [NSLayoutConstraint constraintWithItem:label
                                                                            attribute:NSLayoutAttributeBottom
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:label.superview
                                                                            attribute:NSLayoutAttributeBottom
                                                                           multiplier:1
                                                                             constant:0];
    
    NSLayoutConstraint *labelWidthConstraint = [NSLayoutConstraint constraintWithItem:label
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:nil
                                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                                           multiplier:1
                                                                             constant:39.5];
    
    NSLayoutConstraint *labelHeightConstraint = [NSLayoutConstraint constraintWithItem:label
                                                                             attribute:NSLayoutAttributeHeight
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:nil
                                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                                            multiplier:1
                                                                              constant:15];
    
    
    [label.superview addConstraints:@[labelCenterXConstraint,labelWidthConstraint,labelHeightConstraint,labelBottomConstraint]];
    
}




@end
