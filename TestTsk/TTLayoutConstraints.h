//
//  TTLayoutConstraints.h
//  TestTsk
//
//  Created by ax on 10/27/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TTLayoutConstraints : NSObject

+ (void) constraintForLabel: (UILabel*) label;
+ (void) constraintForButtonLabel: (UILabel*) label;

@end
