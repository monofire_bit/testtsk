//
//  TTViewController.m
//  TestTsk
//
//  Created by ax on 10/27/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import "TTViewController.h"

@interface TTViewController ()

@end

@implementation TTViewController

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    // get link on detail view controller from parent navigation controller
    UIViewController *destinationDetailViewController = [self.splitViewController valueForKey:@"childDetailViewController"];
    
    // present detail view controller
    [self showViewController:destinationDetailViewController sender:self];
}


@end
