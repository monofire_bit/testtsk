//
//  TTServerManager.h
//  TestTsk
//
//  Created by ax on 10/27/15.
//  Copyright © 2015 Oleksii Kushnir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "TTOrder.h"

// request url keys
#define kResourceUrl @"http://mobapply.com/tests/orders/"
#define kGoogleGeocodingBaseUrl @"https://maps.googleapis.com/maps/api/geocode/json?"
#define kGoogleServerKey @"AIzaSyDc0o828q7Ae6ANXj2PWSrv-J2q9xHJzyg"

// notifications
#define ERROR_NOTIFICATION @"ERROR_NOTIFICATION"
#define kNotififactionDict @"kNotififactionDict"

// order instance keys
#define kDepartureAddressKey @"departureAddress"
#define kDestinationAddressKey @"destinationAddress"


#define kZipCodeKey @"zipCode"
#define kCityKey @"city"
#define kCountryCodeKey @"countryCode"
#define kStreetKey @"street"
#define kHouseNumberKey @"houseNumber"


// google geocoding responce keys
#define kResultsKey @"results"
#define kStatusKey @"status"
#define kGeometryKey @"geometry"
#define kLocationKey @"location"
#define kLatKey @"lat"
#define kLngKey @"lng"
#define kFormatted_addressKey @"formatted_address"



typedef enum
{
    DEPARTURE_ADDRESS,
    DESTINATION_ADDRESS
} AddressType;


@interface TTServerManager : NSObject

+ (TTServerManager *) serverManagerSharedInstance;

-(void) requestOrdersFromServerWithBlock: (void (^)(NSArray *ordersArray)) requestSuccess;

-(void) requestLocationForOrder: (TTOrder*) orderElement
                 andAddressType: (AddressType) addressType
                       andBlock: (void (^)(NSDictionary *resultLocationDictionary)) requestSuccess;

@end
